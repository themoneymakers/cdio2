function timeInterestRateFloating = timeInterestRateFloating(dateToday,dates)
% Calculates the time to next payment for FRA and IRS
% Calculates the number of payments left in the IRS/FRA contract
%
% Inputs
%   dateToday - the starting date
%   dates - a vector with dates in rising order 
%
% Outputs
%   timeCount - a vector with the remaining time to all future payments. Has
%   the same size as timePayments
%   numberOfPayments - the number of payments left in the IRS/FRA contract

%------------- BEGIN CODE --------------

timeCount = zeros(length(dates),1);

for i = 1:numberOfPayments
    timeCount(i) = (datesPayments(i)-dateToday)/360;
end

end

%------------- END CODE --------------

