% H�mta data fr�n transf och data collection
function mappingCfRiskMetrics=mappingCfRiskMetrics(element, valuationDate)

%----------step 1: interpolate zero-yield from RiskMetrics cash flows-------------

interpolate(timeToMaturityAcual,timeToMaturityY1,timeToMaturityY2, yield1, yield2);   %Calculates interpolated zero-yield

%----------step 2: calculate PV w/ riskmetrics yields-------------

%indata m�ste inneh�lla matris/vektor med massa info

if (strcmp('element','elementFxForward'))                   %m�ste fixa hur man tar reda p� typ
    actualCF=valuateFxForward(spotPriceCurrency,riskFreeRateDomestic,riskFreeRateForeign, timeToMaturity, maturity);
elseif (strcmp('element','elementFra'))
    actualCF=priceIrs(1,2,3,4,5);                                  
elseif (strcmp('element','elementIrs'))
    actualCF=priceFra(element, spotRate, valuationDate);
end

%--------------priceFra().............
%   spotRate [vector] - Containing two elements. Spot rate for time 0 (time0) and
%   spot rate for time T (timeT).
%   valuationDate [vector] - the dates the FRA should be valued.
%
% Outputs
%   priceFRA [vector] - the price/value of FRA contract at valuationDate
%
            

%----------step 3: interpolate standarddeviation for aacual cash flow-------------

stDeviationCF1;     %h�mta fr�n EWMA 
stDeviationCF2;
%correlation_CF1CF2=calculateCorrelationMatrix(pastReturns, dateVector, valuationTime);        %h�mta fr�n alexandra

stDeviationAcualCF=interpolate(timeToMaturityAcual,timeToMaturityY1,timeToMaturityY2, stDeviationCF1, stDeviationCF2);   

%----------step 4: calcualate allocation (alpha and (1-alpha) for RiskMetrics CF-------------

a=stDeviationCF1^2+stDeviationCF2^2-2*correlation*stDeviationCF1*stDeviationCF2;
b=2*correlation*stDeviationCF1*stDeviationCF2-2*stDeviationCF2^2;
c=stDeviationCF1^2-stDeviationCF2^2

alphaSub=(-b-sqrt(b^2-4*a*b))/(2*a);
alphaAdd=(-b+sqrt(b^2-4*a*b))/(2*a);

if (0=<alphaSub=<1)
    alpha=alphaSub;
else
    alpha=alphaAdd;
end

%----------step 5: calcualate RiskMetrics CF-------------

riskMetricsCF1=alpha*acualCF;
riskMetricsCF2=(1-alpha)*acualCF;

end

function interpolate(timeToMaturityAcualCF,timeToMaturityCF1,timeToMaturityCF2, variableCF1, variableCF2)

interpolatedValue=variableCF1+(variableCF2-variableCF1)*(timeToMaturityAcualCF-timeToMaturityCF1)/(timeToMaturityCF2-timeToMaturityCF1);

end