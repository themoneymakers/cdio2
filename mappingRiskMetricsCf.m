function riskMetricsCf=mappingRiskMetricsCf(element, discountMatrix, valuationDate, dateVector)
% mappingRiskMetricsCf - 
%
% Inputs: valuationDate, issueDate, terminationDate
%       elementIrs - elements containing information about IRS
%       elementFra.issueDate (scalar)
%       elementFra.terminationDate (scalar)
%       elementFra.maturityDate (scalar)
%       elementFra.yield (scalar)
%       elementFra.nominalValue (scalar)
%
% Outputs:
%    riskMetricsCf [matrix] - [riskMetricsCF stDeviationRM]   <-- r�tt volatilitet?
%
% Other m-files required: priceFRA, priceIRS, valuateFxForward, --- etc etc
% MAT-files required: 
% Other files required: 
%
% Author: Kristina Malmgren
% November 2014; Last revision: 09-11-2014

%I nul�get mappas kassafl�den f�r en enhet av ett visst instrument, v�rde m�ste g�ngras med antal tillg�ngar fr�n portf�lj

%------------- BEGIN CODE --------------

valuationNumber=find(buisnessDayList == valuationDate);

if (isa(element,'ForwardRateAgreement')
    calculateRmFra(element,discountMatrix, valuationNumber, valuationDate, terminationDate, dateVector)
elseif (isa(element,'InterestRateSwap')
    calculateRmIrs(element,discountMatrix, valuationNumber, valuationDate, issueDate, terminationDate)
elseif (isa(element,'FxForward')
    calculateRmFxForward(element,discountMatrix, valuationNumber, valuationDate, issueDate, terminationDate)
else
    error('error in mappingMirskMetricsCf, wrong type of element')
end



end

%-------------------- HELP FUNCTIONS-------------------------------------

function riskMetricsCashFlowFra=calculateRmFra(element,discountMatrix, valuationNumber, valuationDate, dateVector)


%       elementFra.issueDate (scalar)
%       elementFra.terminationDate (scalar)
%       elementFra.maturityDate (scalar)
%       elementFra.yield (scalar)
%       elementFra.nominalValue (scalar)

daysToCashFlow(1)=calculateNumberOfBusinessDayBetweenDates(valuationDate,element.maturityDate, dateVector);
daysToCashFlow(1)=calculateNumberOfBusinessDayBetweenDates(valuationDate,element.terminationDate, dateVector);

timeToMaturityRm=[calcTimeToMaturityRm(daysToCashFlow(1)) calcTimeToMaturityRm(daysToCashFlow(2))];

%-------- Calculatats which column the spot rate will be found in------
for i=1:4
timeToMaturityRm(i)=calculateNumberOfBusinessDayBetweenDates(valuationDate, addtodate(valuationDate,timeToMaturityRm(i),'month'), dateVector);
end    

%------- Gets the yields for zero coupon bonds for RiskMetrics vertics----

for i=1:4
    if (timeToMaturityRm(i)==0)
        zeroYields(i)=0;
    else
        zeroYields(i)=spotRateMatrix(valuationNumber, timeToMaturityRm(i));
    end
end

%----Step 1 RiskMetrics: Calculate interpolated yields for maturity and termination days for FRA-----
interpolatedYield(1)=interpolate(daysToCashFlow(1),timeToMaturityRm(1:2), zeroYields(1:2));
interpolatedYield(2)=interpolate(daysToCashFlow(2),timeToMaturityRm(3:4), zeroYields(3:4));

%----Step 2 RiskMetrics: Calculate PV for FRA cashflows-----------------
%presentValueAcualCashFlowFRA=priceFra(element, valuationNumber, interpolatedYield, valuationDate)
presentValueAcualCashFlowFRA=1;

%----Step 3 RiskMetrics: Calculate interpolated volatility FRA-----------------
presentValueBond(1) = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),timeToMaturityRm(1)/12);
presentValueBond(2) = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),timeToMaturityRm(2)/12);

pastReturns(1) = calculatePastReturns(presentValueBond(1));
pastReturns(2) = calculatePastReturns(presentValueBond(2));

pastVolatilityVertic1 = calculateVolatility(pastReturns(1));
pastVolatilityVertic2 = calculateVolatility(pastReturns(2));

pastVolatility=[pastVolatilityVertic1(1) pastVolatilityVertic2(1)];
 
correlationVertics1_2 = calculateCorrelation(pastReturns(1), pastReturns(2));

interpolatedVolatility=interpolate(daysToCashFlow(1),timeToMaturityRm(1:2), pastVolatility);

%----Step 4 RiskMetrics: Calculate allocation alpha & (1-alpha)-----------------

alpha=calculateAlphaRiskMetrics(pastVolatility, interpolatedVolatility, correlationVertics1_2);

riskMetricsCashFlowFra=calculateriskMetricsCf(alpha, presentValueAcualCashFlowFRA);

end


%----------step 4: calcualate allocation (alpha and (1-alpha) for RiskMetrics CF-------------
function alpha=calculateAlphaRiskMetrics(pastVolatility,interpolatedVolatility, pastCorrelationVetics1_2)

a=pastVolatility(1)^2+pastVolatility(2)^2-2*pastCorrelationVetics1_2*pastVolatility(1)*pastVolatility(2)
b=2*pastCorrelationVetics1_2*pastVolatility(1)*pastVolatility(2)-2*pastVolatility(2)^2
c=pastVolatility(2)^2-interpolatedVolatility

alphaSub=(-b-sqrt(b^2-4*a*c))/(2*a)
alphaAdd=(-b+sqrt(b^2-4*a*c))/(2*a)

if (0<=alphaSub<=1)
    alpha=alphaSub;
else
    alpha=alphaAdd;
end

end


%----------step 5: calcualate RiskMetrics CF-------------
function riskMetricsCf = calculateriskMetricsCf(alpha, actualCashFlow)

%H�r ska test g�ras m 5 villkor
    riskMetricsCf(1)=alpha(1)*actualCashFlow;
    riskMetricsCf(2)=(1-alpha(2))*actualCashFlow;

end




%------------Help function: interpolate------------------------------------
%linear interpolation function, used to determaine RiskMetric's yields and volatilities---

function interpolatedValue=interpolate(timeToMaturityAcualCF,timeToMaturityRM, variableRM)

    if (timeToMaturityRM(1)==0)
        interpolatedValue=variableRM(2);        
    else
        interpolatedValue=variableRM(1)+(variableRM(2)-variableRM(1))*(timeToMaturityAcualCF-timeToMaturityRM(1))/(timeToMaturityRM(2)-timeToMaturityRM(1));
    end
end

%---------Help function: timeToMaturityRm----------------------------------
%decides which RiskMetrics vertics the actual cash flow occur inbetween. 

function timeToMaturityRm=calcTimeToMaturityRm(timeToMaturity)

riskMetricsMaturities=[0 1/12 3/12 1/2 1 2 3 4 5 7 9 10 15 20 30];
timeToMaturityRm=[0 0];

j=0;
i=1;
while j==0
    if(360*riskMetricsMaturities(i)<timeToMaturity && timeToMaturity<360*riskMetricsMaturities(i+1))
        timeToMaturityRm=[12*riskMetricsMaturities(i) 12*riskMetricsMaturities(i+1)];
        j=1;
    elseif (timeToMaturity==360*riskMetricsMaturities(i) && timeToMaturity ~=0)
        timeToMaturityRm=[0 12*riskMetricsMaturities(i)];
        j=1;
    elseif (timeToMaturityRm(1)==0 && timeToMaturityRm(2)==0  && i<15)
        i=i+1;
    elseif (timeToMaturity>360*riskMetricsMaturities(15))
        timeToMaturityRm=[0 12*riskMetricsMaturities(15)];
        j=1;
    elseif (timeToMaturity==0)
        timeToMaturityRm=[0 12*riskMetricsMaturities(2)];
        j=1;
    else
        error('error in calcTimeToMaturity')
    end
 end

end




%----------------FX----------------------


%------------Help function: calculate interpolated yields for FX forward---


function yieldsFxForward=interpolateFxForward(currencies, valuationDate, issueDate, terminationDate)

%kanske borde s�tta in datum i

calcTimeToMaturityRm(timeToMaturity)
yieldsRM=[calculateSpotRate(currencies(1), issueDate, terminationDate, valuationDate) calculateSpotRate(currencies(1), issueDate, terminationDate, valuationDate)]; 
    
interpolatedYield=interpolate(timeToMaturityAcual,timeToMaturityRM,yieldsRM);   %Calculates interpolated zero-yield

end



%[spotRateMatrix(valuationNumber, daysToCashFlow) spotRateMatrix(valuationNumber, daysToCashFlow)];


%function cfFxForward=calculateRmFxForward(element, valuationDate, issueDate, terminationDate)

%timeToMaturity=terminationDate-valuationDate;

%H�mta v�xelkurs
%calculateSpotRate('USD', valuationDate, issueDate, terminationDate);

%PV_1 = S0*exp(-interpolatedYieldForeign*timeToMaturity)
%PV_2 = K*exp(-interpolatedYieldDomestic*TimeToMaturity)



