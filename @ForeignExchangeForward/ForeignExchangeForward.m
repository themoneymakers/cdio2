classdef ForeignExchangeForward < FinancialInstrument
    %FOREIGNEXCHANGEFORWARD Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        domesticCurrency;
        foreignCurrency;        
    end
    
    methods
        function FXF = ForeignExchangeForward(rc, id)
            maturityDate = ceilToBusinessDay(addtodate(id,str2double(rc(7:end-1)),'month'));
            FXF@FinancialInstrument(rc,id,maturityDate);
            FXF.domesticCurrency = rc(4:6);
            FXF.foreignCurrency = rc(1:3);          
            
        end
    end
    
end

