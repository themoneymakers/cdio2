function [numOfMissingDates,missingDates] = missingDates(dataBase,dateVector)
% missingDates - Compare a database to a vector with dates to se if date
% are missing
% 
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Examples: 
% >> add3(7)
%
% ans =
%
% 10 
%
% >> add3([2 4])
%
% ans =
%
% 5 7 
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Pontus Ericsson
% November 2014; Last revision: 14-Sep-2014

%------------- BEGIN CODE --------------

numOfMissingDates = 0;
missingDates = [];
for i=1:length(dateVector)
    if ~isKey(dataBase, dateVector(i))
        numOfMissingDates = numOfMissingDates + 1;
        missingDates = [missingDates;dateVector(i)]; % Probably not worth preallocating
    end
end

%------------- END OF CODE --------------

