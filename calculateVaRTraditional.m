
% calculateVaRTraditional - Calculates the VaR with traditional method
% 
%
% Inputs:
%    forwardMatrix [matrix] 
%    Portfolio
%
% Outputs:
%    VaRTraditional [vector] - The VaR using traditional method
%
% Help functions:
%    pastReturn Matrix
%    
%Other Matlab-files required
%calculateRiskMetricsCashFlows

%
% Author: Evelina Stenvall
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------
function traditionalVaR = calculateVaRTraditional(forwardMatrix,Portfolio)%ska portf�ljen ocks� in?
discountMatrix = calculateDiscountMatrix(forwardMatrix);
spotrateMatrix = calculateSpotrateMatrix(forwardMatrix);
STARTDATE = Portfolio.startDate;
ENDDATE = Portfolio.endDate;
valuationNumber = find(businessDayList == STARTDATE);
startNumber = find(businessDayList == STARTDATE);
endNumber = find(businessDayList == ENDDATE);
traditionalVaR=zeros(endNumber-startNumber,1);

for i=startNumber:endNumber
    activeContracts = getActiveContractForPortfolio(Portfolio.instrumentArray,i);
        for j=1:length(activeContracts);% ska det vara activeContracts-1 s� att den sista i calcRMCF �r sigma?
            CF = calculateRiskMetricsCashFlows(activeContracts(j),LIBOR3M,valuationNumber,valuationDate,discountMatrix,spotrateMatrix);%ska �ndras?
            CashFlow = Cashflow + CF;
            CF=0;
        end
    Weights = CashFlow./sum(CashFlows);    
    pastReturnMatrix = calculatePastReturnMatrix(valuationNumber,i);
    correlationMatrix=calculateCorrelationMatrix(pastReturnMatrix); %ska nog �ndras?
    valuationNumber = valuationNumber+1;
    VaRi= Weights.*sigma*1.96;
    VaR = sqrt(VaRi*correlationMatrix*VaRi');
end
traditionalVaR(i-startNumber+1,1)=VaR;
end




%----------------------Past Returns-------------------------
%hj�lpfunktion f�r att ber�kna pastReturnMatrix
function pastReturnMatrix = calculatePastReturnMatrix(valuationNumber,valuationDate)

 pastReturnsOneMonth = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),1/12));
 pastReturnsThreeMonths = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),3/12));
 pastReturnsSixMonths = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),6/12));
 pastReturnsOneYear = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),1));
 pastReturnsTwoYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),2));
 pastReturnsThreeYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),3));
 pastReturnsFourYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),4));
 pastReturnsFiveYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),5));
 pastReturnsSevenYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),7));
 pastReturnsTenYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),10));
 
 pastReturnMatrix=[pastReturnsOneMonth pastReturnsThreeMonths pastReturnsSixMonths pastReturnsOneYear pastReturnsTwoYears pastReturnsThreeYears pastReturnsFourYears pastReturnsFiveYears pastReturnsSevenYears pastReturnsTenYears];
 
end