function priceOfFxForward = priceFxForwardRiskMetrics(elementFxForward, valuationNumber, businessDayList, spotRate, strikePrice, valuationDate)

numberOfInstruments = length(elementFxForward);
priceOfFxForward = zeros(numberOfInstruments,1);

for j = 1:numberOfInstruments
    issueDate = elementFxForward{j}.issueDate;
    maturityDate = elementFxForward{j}.maturityDate;
    spotDate = elementFxForward{j}.spotDate;
    
    issueNumber = find(businessDayList == issueDate);
    
    timeK = (maturityDate - issueDate)/365;
    
    discountFactorK = exp((spotRate(1)-spotRate(2))*timeK)
    strikePriceK = strikePrice(issueDate)*discountFactor
   
    time = (maturityDate - valuationDate)/365;
   
    priceOfFxForward(j) = strikePrice(valuationDate)*exp(-spot(1)*time)-strikePriceK*exp(-spot(2)*time);
     
end

end

function discountRate = findDiscountRate(valuationNumber, spotDate, forwardMatrix, valuationDate, maturityDate)
% Finds discount rate from the discountMatrix
%
% Inputs   
%   valuationNumber - the number of the row corresponding to the date that
%   is valued
%   discountMatrix
%   valuationDate
%   issueDate
%   maturityDate
%   terminationDate
%
% Outputs
%   discountRate - the two discount factors needed for calculating the FRA
%
%------------- BEGIN CODE --------------

if (valuationDate < spotDate)
    startValue = spotDate-valuationDate;
else
    startValue = 0;
end
    coloumnNumber = maturityDate - valuationDate;
    discountRate = sum(forwardMatrix(valuationNumber, startValue:coloumnNumber))/100;
end