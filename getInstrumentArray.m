function instrumentArray = getInstrumentArray( dateContainerMap )
%GETINSTRUMENTARRAY Conversion from containerMap to cell array.
%   Takes an containers.Map as argument and returns and cell array of
%   instruments sortet with respect to maturityDate in ascending order.
%
% Inputs:
%    dateContainerMap - containes.Map with instruments issued a certain date 
%
% Outputs:
%    instrumentArray - cell array with instruments in ascending order with
%    respect to maturityDate
%
% >> getInstrumentArray(usdIrsDataBase(731920));
%
% Other m-files required: instrumentSort.m
% MAT-files required: none
% Other files required: none
%
% Author: Pontus Ericsson
% November 2014; Last revision: -

%------------- BEGIN CODE --------------

    keyArray = keys(dateContainerMap);
    numberOfInstruments = length(keyArray);

    instrumentArray = cell(numberOfInstruments,1);

    for i = 1 : numberOfInstruments
        instrumentArray{i} = dateContainerMap(keyArray{i});
    end
    
    instrumentArray = instrumentSort(instrumentArray, 'maturityDate');

end

%------------- END OF CODE --------------