function [ fixedPeriodLength,floatingPeriodLength] = getInterestRateSwapConvention( ricCode )
%GETINTERESTRATESWAPCONVENTION Conventions for the legs in an IRS
%   Periods of payement of the legs depends on contract length, and
%   at least for japanese IRSes on the reference rate.
%   http://www.opengamma.com/sites/default/files/interest-rate-instruments-and-market-conventions.pdf
%   Can handle following currencies: USD, EUR, GBP, JPY, HKD, SEK
%
% Inputs:
%    ricCode - String with a RIC-code 
%
% Outputs:
%    fixedPeriodLength - Length of the periods for fixed payements
%    floatingPeriodLength - Length of the periods for floating payments
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Pontus Ericsson
% November 2014; Last revision: -

%------------- BEGIN CODE --------------

currency = ricCode(1:3);
referenceRate=ricCode(7); % might be useful to have as a data member
if ricCode(end-1) == 'Y'
    monthsToTermination = str2double(ricCode(8:end-2))*12;
elseif ricCode(end-1) == 'M'
    monthsToTermination = str2double(ricCode(8:end-2));
else
    error('Unknown time format in ric-code')
end

switch currency
    case 'USD'
        fixedPeriodLength = 6;
        floatingPeriodLength = 3;
    case 'EUR'
        fixedPeriodLength = 12;
        if monthsToTermination > 12            
            floatingPeriodLength = 6;
        else
            floatingPeriodLength = 3;
        end
    case 'GBP'
        if monthsToTermination > 12
            fixedPeriodLength = 6;
            floatingPeriodLength = 6;
        else
            fixedPeriodLength = 12;
            floatingPeriodLength = 3;
        end
    case 'JPY'
        fixedPeriodLength = 6;
        if referenceRate == 'T' % TIBOR
            floatingPeriodLength = 3;
        elseif referenceRate == 'L' % LIBOR
            floatingPeriodLength = 6;
        else
            error('Undefined reference rate.')
        end
    case 'HKD'
        fixedPeriodLength = 3;
        floatingPeriodLength = 3;
    case 'SEK'
        fixedPeriodLength = 12;
        floatingPeriodLength = 6;
    otherwise
        error('Undefined currency.')    
end


end

%------------- END OF CODE --------------