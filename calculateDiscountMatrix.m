function [discountMatrix] = calculateDiscountMatrix(forwardMatrix)
% calculateDiscountMatrix - Calculates a cumulative sum of the forward
% matrix
%
% Inputs:
%  forwardMatrix [matrix]  

% Outputs:
%    discountMatrix [matrix] - The spotRate for the whole rate curve
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Johan Hyd�n
% November 2014; Last revision: 17-11-2014

discountMatrix=cumsum(forwardMatrix,2);

end

