function [ noOfActive avgMaturity shortShare] = numberOfActiveContracts( portfolio,date )
% functionName - Returns the number of active contracts in a portfolio on a
% certain date.
%
% Inputs:
%    portfolio - Description
%    date - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Examples: 
% >> numberOfActiveContracts(PortfolioFRAIRSW,735000)
%
% ans =
%
% 216
%
% Other m-files required: Portfolio
% MAT-files required: none
% Other files required: none
%
% Author: Pontus Ericsson
% November 2014; Last revision: 29-Nov-2014

noOfActive = length(getActiveContractForPortfolio(portfolio.instrumentArray,date));

if nargout >= 2
    insArray = getActiveContractForPortfolio(portfolio.instrumentArray,date);
    totalMaturity = 0;
    shortShare = 0;
    for insIter = 1:noOfActive
        totalMaturity=totalMaturity + insArray{insIter}.maturityDate-date;
        if insArray{insIter}.short
            shortShare = shortShare+1;
        end
    end
    avgMaturity = totalMaturity/noOfActive;
    shortShare = shortShare/noOfActive;
end

end

