classdef FinancialInstrument
    %INSTRUMENT Base class for the financial instruments
    %   Base class, might be useful because there is only small difference
    %   between the different instruments. All instruments inheirits
    %   fundamental properties and methods from this class.
    
    properties
        % member data
        ricCode; % RIC-code of base contract
        issueDate; % Date of first data
        spotDate;
        maturityDate;
        nominalValue;
        short;
    end
    
    methods
        function INS = FinancialInstrument(r,id,md) % constructor
            INS.ricCode = r;
            INS.issueDate = id;
            INS.spotDate = ceilToBusinessDay(ceilToBusinessDay(id+1)+1);
            INS.maturityDate = md;
            INS.nominalValue = 1;
            INS.short = false;
        end
        
        %Should it be maturity-issue?
        %or termination-issue?

    end
end

%currency = ricCodeString(1:3); %First three letters of ricCode is the currency