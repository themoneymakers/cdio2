function [gradientVector, hessianMatrix] = calculateGradientHessianIrs(interestRateInstrument, riskFactorEigenMatrix, evaluationDate, businessDayList, spotMatrix)
%>> [a,b]=calculateGradientIrs(testContract, riskFactorEigenMatrix, 731920, businessDayList, spotMatrixUSD)
%
%Fr�gor:
%r(0)T_0=0 just nu i formel?
%Tidsperioden som kontinuerlig tid eller antalet arbetsdagar?
%Vad �r ck?

%Select the number of payments if it is FRA/IRS
if isa(interestRateInstrument,'ForwardRateAgreement')
    paymentDayList = interestRateInstrument.maturityDate;
else    
    paymentDayList = interestRateInstrument.floatDates;
end


if interestRateInstrument.short == false
  multiplier = -1;
else
  multiplier = 1;
end
numberOfPayments = length(paymentDayList);
numberOfRiskFactors = size(riskFactorEigenMatrix,2);
c = ones(numberOfPayments,1);

lengthToSpotDate = max(0,interestRateInstrument.spotDate-evaluationDate);
startDate = max(interestRateInstrument.spotDate, evaluationDate);

%Sets r(0)T_0=0 if the evauationDate is beyond or equal to spotDate,
%otherwise r(T_0)T_0
if (lengthToSpotDate == 0)
  interestRateTZero = 0; %r(T_0)T_0
  eigenVectorTZero = zeros(1,numberOfRiskFactors);
else
  interestRateTZero = extractDateFromRateMatrix(evaluationDate, lengthToSpotDate, businessDayList, spotMatrix);
  eigenVectorTZero = riskFactorEigenMatrix(lengthToSpotDate, :);
end

gradientVector = zeros(numberOfRiskFactors,1);
hessianMatrix = zeros(numberOfRiskFactors);

for k=1:numberOfPayments
  if(paymentDayList(k)>startDate)
    lengthK = calculateNumberOfBusinessDayBetweenDatesImproved(paymentDayList(k), startDate, businessDayList);
    
    gradientVector = gradientVector-multiplier*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*c(k)*exp(interestRateTZero*lengthToSpotDate-extractDateFromRateMatrix(evaluationDate, lengthK, businessDayList, spotMatrix)*lengthK);
    hessianMatrix = hessianMatrix-multiplier*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)*c(k)*exp(interestRateTZero*lengthToSpotDate-extractDateFromRateMatrix(evaluationDate, lengthK, businessDayList, spotMatrix)*lengthK); 
        
  end
end

end