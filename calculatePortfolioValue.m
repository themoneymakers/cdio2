function valueOfPortfolio = calculatePortfolioValue(portfolioArray,lambda)
% calculatePortfolioValue - Calculates total value of portfolio with
% generalized method
%
% Inputs:
%    portfolioArray - Array with all active contracts
%    lambda [vector] - risk factors
%
% Outputs:
%    valueOfPortfolio [scalar] - The value of the portfolio
%
% Help functions:
%    createVech
%    createMatrixD
%   
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 24-11-2014

%------------- BEGIN CODE --------------

numberOfRiskFactors = length(lambda);

%Plocka ut H, g och h

%create vech(lambda'lambda)
vechLambda = createVech(lambda'*lambda);

%create H bar
D = createMatrixD(numberOfRiskFactors):
numberOfContracts = length(portfolioArray);

matrixSizeColoumn = numberOfContracts;
matrixSizeRow = numberOfRiskFactors*(numberOfRiskFactors+1)/2;

hBarVechMatrix = zeros(matrixSizeRow, matrixSizeColoumn);
hBar = zeros(matrixSizeRow, matrixSizeColoumn);

for i = 1:numberOfContracts
    hBarVechMatrix(:,i) = createVech(H(i));
end    

hBar = D*hBarVechMatrix;

%Calculate G

for j = 1:numberOfContracts
    G(:,j) = hBarVechMatrix(:,i) = createVech(H(i));
end   


%Calculate value of portfolio

valueOfPortfolio = lambda'*G+1/2*vechLambda'*hBar;

end

function vech = createVech(symmetricMatrix)
% createVech - Creates vech operator of symmetric matrix
%
% Inputs:
%    symmetricMatrix [matrix]- symmetric matrix that are going to be
%    transformed 
%
% Outputs:
%    vech [vector] - the veck operator
%

    matrixDimension = length(symmetricMatrix);

    startOfVector = 1;
    sizeOfVector = matrixDimension;

    vech = zeros(sum([1:matrixDimension]),1);

    for i=1:matrixDimension
        vech(startOfVector:sizeOfVector+startOfVector-1)=symmetricMatrix(i:end,i);

        startOfVector = startOfVector+sizeOfVector;
        sizeOfVector=sizeOfVector-1;
    end
    
end

function D = createMatrixD(numberOfRiskFactors)
% createMatrixD - created the diagonal matrix D
%
% Inputs:
%    numberOfRiskFactors [scalar]
% Outputs:
%    D [matrix] - the matrix D that are used to calculate the value of
%    portfolio
%

sizeOfDMatrix = numberOfRiskFactors*(numberOfRiskFactors+1)/2;
vectorD = zeros(1,sizeOfDMatrix)
for i=1:sizeOfDMatrix
    equationTrue = 0;
    for j = 1:numberOfRiskFactors
        if (i == j*(2*numberOfRiskFactors+3-j)/2-numberOfRiskFactors)
            equationTrue = 1;
        end
    end
    if (equationTrue == 1)
        vectorD(i) = 1;
    else
        vectorD(i) = 2;
    end
end

D = diag(vectorD)
   
end