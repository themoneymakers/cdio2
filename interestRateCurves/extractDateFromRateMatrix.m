function [interestRate] = extractDateFromRateMatrix(date, daysForward, businessDayList, matrix)
% extractDateFromRateMatrix - Extract interestRate from a matrix based on a
%                             datenum
%
% Inputs:
%  date [datenum] - specifies the specific date wanted
%  daysForward - is the days forward from date above
%  businessDayList - Lists the business days in matrix
%  matrix - The matrix of interest rates

% Outputs:
%  interestRate [matrix] - The interestRate for the specific day
%
% Example:
% >> extractDateFromRateMatrix(726835, 1, businessDayList,forwardMatrixUSD)
% 
% ans =
% 
%     0.0800
%
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Emil Karlsson
% November 2014; Last revision: 20-11-2014
row = find(businessDayList==date);
if row
  interestRate = matrix(row, daysForward);
else
  error('Date does not exist in businessDayList')
end

end
    