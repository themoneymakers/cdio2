function [discountRate] = calculateDiscountRate(dates, forwardMatrix, valuationDate, startDate, endDate)

dateNumbers = datenum(dates);

valuationNumber = find(dateNumbers == valuationDate);
startNumber = find(dateNumbers == startDate);

discountRate = 0;
%numberOfDays

numberOfDays = datenum(endDate) - datenum(startDate);

if (numberOfDays > 1)
    discountRate=sum(forwardMatrix(valuationNumber,startNumber-valuationNumber+1:startNumber-valuationNumber+numberOfDays));
else
    error('Wrong dates')
end
