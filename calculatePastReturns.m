function pastReturns = calculatePastReturns(valueOfInstrument)
% calculatePastReturns - Calculates the log-returns
% Uses past values of instruments to calculate past returns
%
% Inputs:
%    valueOfInstrument [matrix] - values of instrument over a time period,
%    each coloumn is an asset, and each row is a time (from newest
%    to oldest)
%
% Outputs:
%    pastReturns [matrix] - The log-returns of each asset at each time
%
% Other m-files required: calculatePriceFra, calculatePriceIrs (...etc)
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------

[numberOfData numberOfAssets] = size(valueOfInstrument); 
pastReturns = zeros(numberOfData-1,numberOfAssets);

for i = 1:length(valueOfInstrument)-1
    pastReturns(i,:) = log(valueOfInstrument(i,:)./valueOfInstrument(i+1,:));
end

end

%------------- END OF CODE --------------