function [valueAtRisk expectedShortfall] = simulateGeneralMethodDistribution(gVector, matrixHessian, meanVector, covarianceMatrix, numberOfIterations)
% functionName - One line description of what the function performs (H1 line)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Examples: 
% >> simulateGeneralMethodDistribution(ones(20,1), ones(20)*10, zeros(20,1), 10*eye(20), 1000000)
%
% ans =
%
% 10 
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: FirstName FamilyName
% September 2014; Last revision: 14-Sep-2014

%------------- BEGIN CODE --------------
% % Regular
% result = zeros(numberOfIterations,1);
% randomMatrix = mvnrnd(meanVector, covarianceMatrix, numberOfIterations)';
% for i=1:numberOfIterations
%     randomMatrixObservation = randomMatrix(:,i);
%     result(i,1) = gVector'*randomMatrixObservation+1/2.*randomMatrixObservation'*matrixHessian*randomMatrixObservation;
% end
% 
% valueAtRisk = prctile(result, 0.05);


% Hupercube
result = zeros(numberOfIterations,1);
normalHyperCube = lhsnorm(meanVector, covarianceMatrix, numberOfIterations)';

for i=1:numberOfIterations
    randomMatrixObservation = normalHyperCube(:,i);
    result(i,1) = gVector'*randomMatrixObservation+1/2.*randomMatrixObservation'*matrixHessian*randomMatrixObservation;
end

if nargout<2
    valueAtRisk = -prctile(result, 5); % Var on 5-percent level
elseif nargout==2 % Only do this if needed (prtctile may be faster to get Var, but we need to sort to calculate ES)
    sortedResult=sort(result); %Sort in ascending order
    limit = ceil(0.05*numberOfIterations)+1;
    valueAtRisk = -sortedResult(limit);
    expectedShortfall = -mean(sortedResult(1:limit));
end

disp('###################################################################')


end


%------------- END OF CODE --------------

