%Startup script
tic
clear;
close all;
clc;
load('BlomvallTransformationUSD1.mat');
load('businessDayList.mat');
spotMatrixUSD = calculateSpotRateMatrix(forwardMatrixUSD);
[ riskFactorUSD, e, change, dailyChange ]=determineRiskFactors(6,forwardMatrixUSD);
riskFactorUSD = calculateSpotRateMatrix(riskFactorUSD')';
meanVector = mean(dailyChange);
covarianceMatrix = cov(dailyChange);

load('Portfolios\PortfolioFRAIRSW.mat') % Load portfolio

toc