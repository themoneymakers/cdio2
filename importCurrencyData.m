% Script to import data on currencies from Excel. Conventions on how
% currencies are quoted makes it difficult to write this script as a
% function e.g. 'EUR=' quotes the price of 1 � in $ while 'JPY=' quotes the
% price of 1 $ in yen.
%
% Because there are no arbitrage, it should be sufficient to import data on
% all currencies quoted agains US dollar e.g.
% (EUR/SEK)=(EUR/USD)*(USD/SEK).


clc,clear,close all



tic
[numerical, text] = xlsread('mostTradedCurrencies.xlsx', 'USD');
toc

tic
numberOfCurrencyPairs = (size(numerical,2)+1)/2


% Euro
eurSpotList = numerical(:,1);
eurSpotList = eurSpotList(isfinite(eurSpotList(:, 1)), :);  % Remove NaNs
eurDateList = text(4:end,2);
eurTimeStamp = datenum(eurDateList(1:size(eurSpotList,1),1),'yyyy-mm-dd');

curEURUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(eurSpotList)
    curEURUSD(eurTimeStamp(i)) = eurSpotList(i);
end

% Japanese yen
jpySpotList = numerical(:,3);
jpySpotList = jpySpotList(isfinite(jpySpotList(:, 1)), :);  % Remove NaNs
jpySpotList = 1./jpySpotList; % 'JPY=' is the price of one dollar in yen
jpyDateList = text(4:end,4);
jpyTimeStamp = datenum(jpyDateList(1:size(jpySpotList,1),1),'yyyy-mm-dd');

curJPYUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(jpySpotList)
    curJPYUSD(jpyTimeStamp(i)) = jpySpotList(i);
end

% Pound sterling
gbpSpotList = numerical(:,5);
gbpSpotList = gbpSpotList(isfinite(gbpSpotList(:, 1)), :);  % Remove NaNs
gbpDateList = text(4:end,6);
gbpTimeStamp = datenum(gbpDateList(1:size(gbpSpotList,1),1),'yyyy-mm-dd');

curGBPUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(gbpSpotList)
    curGBPUSD(gbpTimeStamp(i)) = gbpSpotList(i);
end

% Aussie
audSpotList = numerical(:,7);
audSpotList = audSpotList(isfinite(audSpotList(:, 1)), :);  % Remove NaNs
audDateList = text(4:end,8);
audTimeStamp = datenum(audDateList(1:size(audSpotList,1),1),'yyyy-mm-dd');

curAUDUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(audSpotList)
    curAUDUSD(audTimeStamp(i)) = audSpotList(i);
end

% Swiss Franc
chfSpotList = numerical(:,9);
chfSpotList = chfSpotList(isfinite(chfSpotList(:, 1)), :);  % Remove NaNs
chfSpotList = 1./chfSpotList; % 'CHF=' is the price of one dollar in swiss franc
chfDateList = text(4:end,10);
chfTimeStamp = datenum(chfDateList(1:size(chfSpotList,1),1),'yyyy-mm-dd');

curCHFUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(chfSpotList)
    curCHFUSD(chfTimeStamp(i)) = chfSpotList(i);
end

% Canadian dollar
cadSpotList = numerical(:,11);
cadSpotList = cadSpotList(isfinite(cadSpotList(:, 1)), :);  % Remove NaNs
cadSpotList = 1./cadSpotList; % 'cad=' is the price of one USD in CAD
cadDateList = text(4:end,12);
cadTimeStamp = datenum(cadDateList(1:size(cadSpotList,1),1),'yyyy-mm-dd');

curCADUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(cadSpotList)
    curCADUSD(cadTimeStamp(i)) = cadSpotList(i);
end

% Mexican Peso
% ~100 early dates missing
mxnSpotList = numerical(:,13);
mxnSpotList = mxnSpotList(isfinite(mxnSpotList(:, 1)), :);  % Remove NaNs
mxnSpotList = 1./mxnSpotList; % 'mxn=' is the price of one USD in MXN
mxnDateList = text(4:end,14);
mxnTimeStamp = datenum(mxnDateList(1:size(mxnSpotList,1),1),'yyyy-mm-dd');

curMXNUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(mxnSpotList)
    curMXNUSD(mxnTimeStamp(i)) = mxnSpotList(i);
end

% Chinese Renminbi
% ~3000 early dates are missing.
cnySpotList = numerical(:,15);
cnySpotList = cnySpotList(isfinite(cnySpotList(:, 1)), :);  % Remove NaNs
cnySpotList = 1./cnySpotList; % 'CNY=' is the price of one USD in CNY
cnyDateList = text(4:end,16);
cnyTimeStamp = datenum(cnyDateList(1:size(cnySpotList,1),1),'yyyy-mm-dd');

curCNYUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(cnySpotList)
    curCNYUSD(cnyTimeStamp(i)) = cnySpotList(i);
end

% Kiwi
nzdSpotList = numerical(:,17);
nzdSpotList = nzdSpotList(isfinite(nzdSpotList(:, 1)), :);  % Remove NaNs
nzdDateList = text(4:end,18);
nzdTimeStamp = datenum(nzdDateList(1:size(nzdSpotList,1),1),'yyyy-mm-dd');

curNZDUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(nzdSpotList)
    curNZDUSD(nzdTimeStamp(i)) = nzdSpotList(i);
end

% Swedish Krona
sekSpotList = numerical(:,19);
sekSpotList = sekSpotList(isfinite(sekSpotList(:, 1)), :);  % Remove NaNs
sekSpotList = 1./sekSpotList; % 'SEK=' is the price of one USD in SEK
sekDateList = text(4:end,20);
sekTimeStamp = datenum(sekDateList(1:size(sekSpotList,1),1),'yyyy-mm-dd');

curSEKUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(sekSpotList)
    curSEKUSD(sekTimeStamp(i)) = sekSpotList(i);
end

% Russian Ruble
% ~1500 early dates missing
rubSpotList = numerical(:,21);
rubSpotList = rubSpotList(isfinite(rubSpotList(:, 1)), :);  % Remove NaNs
rubSpotList = 1./rubSpotList; % 'RUB=' is the price of one USD in RUB
rubDateList = text(4:end,22);
rubTimeStamp = datenum(rubDateList(1:size(rubSpotList,1),1),'yyyy-mm-dd');

curRUBUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(rubSpotList)
    curRUBUSD(rubTimeStamp(i)) = rubSpotList(i);
end

% Hong Kong Dollar
hkdSpotList = numerical(:,23);
hkdSpotList = hkdSpotList(isfinite(hkdSpotList(:, 1)), :);  % Remove NaNs
hkdSpotList = 1./hkdSpotList; % 'HKD=' is the price of one USD in HKD
hkdDateList = text(4:end,24);
hkdTimeStamp = datenum(hkdDateList(1:size(hkdSpotList,1),1),'yyyy-mm-dd');

curHKDUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(hkdSpotList)
    curHKDUSD(hkdTimeStamp(i)) = hkdSpotList(i);
end

% Singapore Dollar
sgdSpotList = numerical(:,25);
sgdSpotList = sgdSpotList(isfinite(sgdSpotList(:, 1)), :);  % Remove NaNs
sgdSpotList = 1./sgdSpotList; % 'SGD=' is the price of one USD in SGD
sgdDateList = text(4:end,26);
sgdTimeStamp = datenum(sgdDateList(1:size(sgdSpotList,1),1),'yyyy-mm-dd');

curSGDUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(sgdSpotList)
    curSGDUSD(sgdTimeStamp(i)) = sgdSpotList(i);
end

% Turkish Lira
% ~100 early dates missing
trySpotList = numerical(:,27);
trySpotList = trySpotList(isfinite(trySpotList(:, 1)), :);  % Remove NaNs
trySpotList = 1./trySpotList; % 'TRY=' is the price of one USD in TRY
tryDateList = text(4:end,28);
tryTimeStamp = datenum(tryDateList(1:size(trySpotList,1),1),'yyyy-mm-dd');

curTRYUSD = containers.Map('keyType','double','ValueType','double');

for i = 1:length(trySpotList)
    tryTimeStamp(i)
    curTRYUSD(tryTimeStamp(i)) = trySpotList(i);
end

toc